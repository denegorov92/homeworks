package ru.egorov.hw20;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

public class Json {
    public static void main(String[] args) throws IOException {


        try {
            URL url = new URL("https://www.metaweather.com/api/location/search/?query=san");
            try (InputStream is = url.openStream();
                 Reader reader = new InputStreamReader(is);
                 BufferedReader br = new BufferedReader(reader)) {
                String line = br.readLine();
                System.out.println(line);
                while ((line = br.readLine()) != null) {
                    System.out.println(line);
                }
            } catch (IOException e) {
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
