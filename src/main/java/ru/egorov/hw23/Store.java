package ru.egorov.hw23;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Store implements Basket {
    private List<Item> store = new LinkedList<>();

    public static void main(String[] args) {
        Store stores = new Store();

        stores.addProduct("milk", 1);
        stores.addProduct("bear", 3);
        stores.addProduct("egg", 2);
        showStore("Добавили элементов", stores.store);
        stores.removeProduct("milk");
        showStore("Удалили элемент ", stores.store);
        stores.updateProductQuantity("egg", 3);
        showStore("Увеличили кол-во ", stores.store);
        System.out.println("В нашей корзине есть продукты" + stores.getProducts());
        stores.getProductQuantity("bear");

    }

    private static void showStore(String title, List<Item> store) {
        System.out.println(title);
        for (Item item : store) {
            System.out.println("Item: " + item);
        }
        System.out.println();
    }

    @Override
    public void addProduct(String product, int quantity) {
        for (Item item : store) {
            if (item.getName().equals(product)) {
                item.addQuantity(quantity);
            }
        }
        store.add(new Item(product, quantity));
    }

    @Override
    public void removeProduct(String product) {
        for (Iterator<Item> iterator = store.iterator(); iterator.hasNext(); ) {
            Item item = iterator.next();
            if (item.getName().equals(product)) {
                iterator.remove();
            }
        }
    }

    @Override
    public void updateProductQuantity(String product, int quantity) {

        for (Item item : store) {
            if (item.getName().equals(product)) {
                item.addQuantity(quantity);
            }
        }
    }

    @Override
    public void clear() {
        store.clear();
    }

    @Override
    public List<String> getProducts() {
        List<String> list = new ArrayList<>(store.size());
        for (Item item : store) {
            list.add(item.getName());
        }
        return list;
    }

    @Override
    public int getProductQuantity(String product) {
        for (Item item : store) {
            if (item.getName().equals(product)) {
                System.out.println("В нашей корзине сейчас есть : " + product + " в кол-ве " + item.getQuantity() + " штук");
            }
        }
        return 0;
    }
}


