package ru.egorov.hw15;

import java.io.File;
import java.io.IOException;

public class Recursion {
    public static void main(String[] args) {
        createTestData();
        System.out.println(find(new File("testData2")));
    }

    private static long find(File root) {
        long result = 0;
        if (root != null && root.exists()) {
            if (root.isDirectory()) {
                for (File file : root.listFiles()) {
                    result = find(file);
                }
            }
            System.out.println(root.toPath());
        }
        return 0;
    }

    private static void createTestData() {
        try {

            new File("testData2/a/b/c/d").mkdirs();
            new File("testData2/a/b/c/d/1.txt").createNewFile();
            new File("testData2/a/c/a/a").mkdirs();
            new File("testData2/b/a/a").mkdirs();
            new File("testData2/b/c/b").mkdirs();
            new File("testData2/b/c/2.txt").createNewFile();
            new File("testData2/a/3.txt").createNewFile();
            new File("testData2/b/4.txt").createNewFile();


        } catch (Exception e) {
        }
    }

}
