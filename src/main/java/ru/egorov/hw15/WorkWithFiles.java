package ru.egorov.hw15;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class WorkWithFiles {
    public static void main(String[] args) {
        File dir = new File("testData/a/b");
        System.out.println("создаем директории " + dir.getPath() + " " +
                dir.mkdirs());

//Создаем файл
        File file = new File("testData/a/b/123.txt");
        try {
            System.out.println("создали файл [" + file.getAbsolutePath() +
                    "] " + file.createNewFile());
        } catch (IOException ex) {
            System.out.println("Файл не был создан " + ex.getMessage());
        }

//Переименовываем файл
        File file1 = new File("testData/a/b/456.txt");
        if (file.renameTo(file1)) {
            System.out.println("Файл переименован успешно");
            ;
        } else {
            System.out.println("Файл не был переименован");
        }

//Копируем файл
        File file2 = new File("testData/a/456.txt");
        try {
            Files.copy(file1.toPath(), file2.toPath());
        } catch (IOException ex) {
            System.out.println("Файл не был создан " + ex.getMessage());
        }

//Удаляем файл и директорию
        System.out.println("удаляем " + file1.delete() + " " + dir.delete());


        listDirectory(new File("testData/"));
    }

    private static void listDirectory(File files) {
        System.out.println("Содержимое каталога " + files.getPath());
        for (File file : files.listFiles()) {
            System.out.println(file.getName());
        }
    }
}
