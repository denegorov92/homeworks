package ru.egorov.hw4;

import java.util.Scanner;

public class progression {
    public static void main(String[] args) {
        System.out.println("Вывод прогрессий");
        System.out.println();
        // Ввод пользовательских данных
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите первое число: ");
        int first = scan.nextInt();
        System.out.print("Введите шаг прогрессии: ");
        int step = scan.nextInt();
        System.out.print("Введите количество членов прогрессии: ");
        int number = scan.nextInt();
        // Расчёт и вывод прогрессий
        System.out.println();
        // Арифметическая прогрессия
        System.out.println("Арифметическая прогрессия");
        for (int i = 0; i < number; i++) {
            System.out.print(first + (step * i) + " ");
        }
        System.out.println();
        // Геометрическая прогрессия
        System.out.println("Геометрическая прогрессия");
        for (int i = 0; i < number; i++) {
            double a = first * Math.pow(step, i);
            System.out.printf("%.0f ", a);
        }
        System.out.println();
    }
}
