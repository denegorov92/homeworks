package ru.egorov.hw4;

public class multiplication {
    public static void main(String[] args) {
        int[][] twoDimArray = new int[10][10];
        System.out.println("Таблица умножения:");
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                twoDimArray[i][j] = (i + 1) * (j + 1);
                // Вывод значений таблицы умножения
                if (twoDimArray[i][j] < 10) {
                    System.out.print(twoDimArray[i][j] + "   ");
                } else {
                    if (twoDimArray[i][j] < 100) {
                        System.out.print(twoDimArray[i][j] + "  ");
                    } else {
                        System.out.print(twoDimArray[i][j]);
                    }
                }
            }
            System.out.println();
        }
    }
}
