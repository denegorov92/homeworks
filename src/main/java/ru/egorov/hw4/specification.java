package ru.egorov.hw4;

import java.util.Scanner;

public class specification {
    public static void main(String[] args) {
        System.out.println("Введите число");
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        System.out.println("Характеристики числа:");
        if (num > 0) {
            System.out.println("Положительное число");
        } else {
            if (num < 0) {
                System.out.println("Отрицательное число");
            } else {
                System.out.println("Нулевое число");
            }
        }
        if (num != 0) {
            if (num % 2 == 0) {
                System.out.println("Четное число");
            } else {
                System.out.println("Нечетное число");
            }
        }
    }
}
