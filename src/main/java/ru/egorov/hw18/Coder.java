package ru.egorov.hw18;

import java.io.*;
import java.nio.charset.Charset;

public class Coder {
    public static void main(String[] args) throws IOException {

        String helloWorld = "Привет Мир!";
        saveToFile("cp866.txt", helloWorld, "cp866");
        copyToFile("utf-8.txt", readForFile("cp866.txt", "cp866"), "utf-8");
    }

    private static void saveToFile(String outFile, String helloWorld, String charsetName) throws IOException {
        try (FileOutputStream os = new FileOutputStream(outFile);) {
            os.write(helloWorld.getBytes(Charset.forName(charsetName)));
        }

    }

    private static void copyToFile(String outFile, String copy, String charsetName) throws IOException {
        try (FileOutputStream os = new FileOutputStream(outFile);) {
            os.write(copy.getBytes(Charset.forName(charsetName)));
        }

    }

    public static String readForFile(String filename, String encoding) throws IOException {
        String result = "";
        try (InputStreamReader isr = new InputStreamReader(
                new FileInputStream(filename), encoding)) {
            char[] buffer = new char[4];
            int size;
            while ((size = isr.read(buffer)) > 0) {
                result += new String(buffer, 0, size);
            }

            return result;

        }
    }
}
