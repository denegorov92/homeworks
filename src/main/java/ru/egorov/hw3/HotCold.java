package ru.egorov.hw3;

import java.util.Scanner;
import java.util.Random;

public class HotCold {
    public static void main(String[] args) {
        Random random = new Random(System.currentTimeMillis());
        int num = random.nextInt(101);
        int raznitsa = 0;
        int oldraznitsa = 0;

        Boolean NextBoolean = true;
        while (NextBoolean) {
            Scanner scan = new Scanner(System.in);
            System.out.print("Введите число: ");
            int number = scan.nextInt();
            oldraznitsa = raznitsa;
            raznitsa = (Math.abs((number) - num));
            if (num == number)
            {
                System.out.println("Вы угадали!");
                System.exit(0);
            }
            // Сообщим пользователю "Холодно" или "Горячо"
            if (raznitsa > oldraznitsa)
                {
                    System.out.println("Холодно");
                }
                else
                {
                    System.out.println("Горячо");
                }
            // Обработаем выход из программы
            System.out.println("Для выхода введите 0, для продолжения любую другую клавишу");
            Scanner enteranswer = new Scanner(System.in);
            int answer = enteranswer.nextInt();
            if (answer == 0) { NextBoolean = false; }
        }
    }
}
