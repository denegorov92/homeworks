package ru.egorov.hw31;

public class Palindrome {
    public static void main(String[] args) {

        String line = "мадам";
        String line2 = "шалаш";
        String line3 = "шаверма";

        System.out.println("Палиндром? " + isPalindrome(line));
        System.out.println("Палиндром? " + isPalindrome2(line2));
        System.out.println("Палиндром? " + isPalindrome2(line3));
    }

    private static Boolean isPalindrome(String s) {
        return s.equals(new StringBuilder(s).reverse().toString());
    }

    private static Boolean isPalindrome2(String s) {
        for (int i = 0; i < s.length() / 2; ++i) {
            if (s.charAt(i) != s.charAt(s.length() - i - 1)) {
                return false;
            }
        }
        return true;
    }
}
