package ru.egorov.hw6;

class AutoWorks {
    Difficulty[] difficulties;
    CarWash[] carWashes;
    TO[] TOx;

    AutoWorks() {
        difficulties = new Difficulty[1];
        difficulties[0] = new Difficulty(1);
        carWashes = new CarWash[1];
        carWashes[0] = new CarWash(500);
        TOx = new TO[1];
        TOx[0] = new TO(1000);

    }

    public AutoWorks(int size) {
    }

    int sumOfDifficulty() {
        int sum = 0;
        for (Difficulty m : difficulties) {
            sum += m.getCost();
        }
        return sum;

    }

    int sumOfCarWash() {
        int sum = 0;
        for (CarWash m : carWashes) {
            sum += m.getSize();
        }
        return sum;
    }

    int sumOfTO() {
        int sum = 0;
        for (TO m : TOx) {
            sum += m.getNumber();
        }
        return sum;
    }

    public int AutoWorksPrice() {
        return sumOfDifficulty() * sumOfCarWash() + sumOfTO();

    }
}

