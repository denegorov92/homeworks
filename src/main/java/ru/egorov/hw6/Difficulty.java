package ru.egorov.hw6;

class Difficulty {
    private int cost;

    Difficulty(int cost) {
        this.cost = cost;
    }

    int getCost() {
        return cost;
    }
}
