package ru.egorov.hw6;

public class CarWash extends AutoWorks {
    public int size;

    public CarWash(int size) {
        super(size);
        this.size = size;

    }

    public int getSize() {
        return size;
    }
}
