package ru.egorov.hw14;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Scanner;


public class App {
    static Logger logger = LoggerFactory.getLogger(App.class);
    private static VM vm = new VM();

    public static void main(String[] args) {
        logger.trace("Запуск приложения");


        System.out.println("Меню: ");
        for (String line : vm.getDrinkTypes()) {

            System.out.println(line);
        }

        Scanner scan = new Scanner(System.in);
        printHelp();
        while (scan.hasNext()) {
            String command = scan.next();
            logger.info("Пользователь ввел: {}", command);
            switch (command) {
                case "add": {
                    int money = scan.nextInt();
                    processAddMoney(money);
                    break;
                }
                case "get": {
                    int key = scan.nextInt();
                    processGetDrink(key);
                    break;
                }
                case "end": {
                    processEnd();
                    return;
                }
                default:
                    System.out.println("Неверный ввод");
                    logger.warn("Неверный ввод");
            }
            scan.nextLine();
        }

    }

    private static void processAddMoney(int money) {
        System.out.println("Внесено: " + vm.addMoney(money));
    }

    private static void processGetDrink(int key) {
        Info drinkType = vm.giveMeADrink(key);
        if (drinkType != null) {
            System.out.println("Ваш выбор: " + drinkType.getName());
        } else {
            System.out.println("Недостаточно средств");
            logger.warn("Недостаточно средств");
        }
    }

    private static void processEnd() {
        logger.info("Выдана сдача", vm.getChange());
        System.out.println("Ваша сдача: " + vm.getChange());
        logger.trace("Выход из приложения");
    }

    private static void printHelp() {
        logger.info("Список команд");
        System.out.println("Введите 'add <количество>' для добавления купюр");
        System.out.println("Введите 'get <код напитка>' для получения напитка");
        System.out.println("Введите 'end' для получения сдачи");
    }

}

