package ru.egorov.hw14;


public enum Drinks implements Info {
    TEQUILA("Текила", 1000),
    VODKA("Водка", 500),
    RUM("Ром", 1100),
    COGNAC("Коньяк", 700);

    private final String name;
    private final int price;

    Drinks(String name, int price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getPrice() {
        return price;
    }

}

