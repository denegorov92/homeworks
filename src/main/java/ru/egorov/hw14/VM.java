package ru.egorov.hw14;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VM {
    private final static Logger logger = LoggerFactory.getLogger(VM.class);


    private int money = 0;
    private Product[] drinks = new Product[]{
            new Product(Drinks.TEQUILA),
            new Product(Drinks.VODKA),
            new Product(Drinks.RUM),
            new Product(Drinks.COGNAC),
    };

    public int addMoney(int money) {

        this.money += money;
        return this.money;
    }

    public Info giveMeADrink(int key) {
        if (!isKeyValid(key)) {
            return null;
        }

        Product selected = drinks[key];
        if (!isMoneyEnough(selected)) {
            return null;
        }

        Info drink = selected.take();
        money -= drink.getPrice();
        return drink;
    }

    public String[] getDrinkTypes() {
        String[] result = new String[drinks.length];
        for (int i = 0; i < drinks.length; i++) {
            result[i] = String.format("%d - %6s: %6d руб", i, drinks[i].getName(), drinks[i].getPrice());
        }
        return result;
    }

    private boolean isMoneyEnough(Product selected) {
        return money >= selected.getPrice();
    }

    private boolean isKeyValid(int key) {
        return key >= 0 && key < drinks.length;
    }

    public int getChange() {
        int money = this.money;
        this.money = 0;
        return money;
    }
}

