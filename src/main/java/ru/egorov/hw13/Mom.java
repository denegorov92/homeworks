package ru.egorov.hw13;

import java.io.IOException;


public class Mom {
    public static void main(String[] args) throws IOException {
        String[] products = {"Каша", "Омлет", "Чикенбургер", "Блины"};


        System.out.println("Что сегодня на завтрак?");
        for (int i = 0; i < products.length; i++) {
            System.out.println(i + "\t" + products[i]);
        }

        try {

            Child.eat();
            System.out.println("Съел за обе щеки");
        } catch (IOException e) {

        } finally {
            System.out.println("Спасибо");
        }


    }
}
