package ru.egorov.hw13;

import java.io.IOException;
import java.util.Scanner;

public class Child {
    public static void eat() throws IOException {
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        if (x != 3) {
            throw new IOException();
        }

    }
}

