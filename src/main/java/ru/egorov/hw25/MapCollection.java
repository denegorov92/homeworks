package ru.egorov.hw25;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class MapCollection {
    public static void main(String[] args) {

        Map<String, String> person = createMap();
        System.out.println("Создали коллекцию, где есть одинаковые значения: \n" + createMap());
        System.out.println(isUnique(person));
        Map<String, String> person2 = createMap2();
        System.out.println("Создали коллекцию, где нет одинаковых значений: \n" + createMap2());
        System.out.println(isUnique(person2));
    }

    private static Map<String, String> createMap() {
        Map<String, String> person = new HashMap<>();
        person.put("Вася", "Иванов");
        person.put("Петр", "Петров");
        person.put("Виктор", "Сидоров");
        person.put("Сергей", "Савельев");
        person.put("Вадим", "Викторов");
        return person;
    }

    private static Map<String, String> createMap2() {
        Map<String, String> person2 = new HashMap<>();
        person2.put("Вася", "Иванов");
        person2.put("Петр", "Петров");
        person2.put("Виктор", "Иванов");
        person2.put("Сергей", "Савельев");
        person2.put("Вадим", "Петров");
        return person2;
    }

    private static boolean isUnique(Map<String, String> map) {

        HashMap<String, String> copy = new HashMap<>(map);
        for (Map.Entry<String, String> pair : copy.entrySet()) {
            int freqeuncy = Collections.frequency(copy.values(), pair.getValue());
            if (freqeuncy > 1) {
                System.out.println("Найдено больше 1-го одинакового значения");
                return false;
            }
        }
        System.out.println("Одинаковые значения не найдены");
        return true;
    }
}
