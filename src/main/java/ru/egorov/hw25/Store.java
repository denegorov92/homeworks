package ru.egorov.hw25;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Store implements Basket {

    private Map<String, Integer> store = new HashMap<>();

    public static void main(String[] args) {
        Store methods = new Store();

        methods.addProduct("Milk", 2);
        methods.addProduct("Egg", 1);
        methods.addProduct("Bear", 3);
        showStore("Добавили продуктов в мапу", methods.store);

        methods.removeProduct("Bear");
        showStore("Удалили продукт из мапы", methods.store);

        methods.updateProductQuantity("Egg", 3);
        showStore("Изменили количество", methods.store);
        System.out.println("Вызов метода с информацией о содержании мапы");
        methods.getProducts();
        System.out.println("Вызов метода с информацией о количестве определенного продукта в мапе");
        methods.getProductQuantity("Egg");
    }

    private static void showStore(String title, Map<String, Integer> store) {
        System.out.println(title);
        for (Map.Entry<String, Integer> entry : store.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
    }

    @Override
    public void addProduct(String product, int quantity) {
        store.put(product, quantity);
    }

    @Override
    public void removeProduct(String product) {
        Map<String, Integer> copy = new HashMap<>(store);
        for (Map.Entry<String, Integer> map : copy.entrySet()) {
            if (map.getKey().equals(product)) {
                store.remove(product);
                break;
            } else {
                try {
                    throw new Exception();
                } catch (Exception e) {
                    System.out.println("Продукта " + product + " нет в корзине,удалить нельзя");
                    break;
                }
            }
        }
    }

    @Override
    public void updateProductQuantity(String product, int quantity) {
        for (Map.Entry<String, Integer> entry : store.entrySet()) {
            if (entry.getKey().equals(product)) {
                store.put(product, quantity);
                break;
            } else {
                try {
                    throw new Exception();
                } catch (Exception e) {
                    System.out.println("Продукта " + product + " нет в корзине,изменить кол-во нельзя");
                    break;
                }
            }
        }
    }

    @Override
    public void clear() {
        store.clear();
    }

    @Override
    public List<String> getProducts() {
        Set<Map.Entry<String, Integer>> set = store.entrySet();
        for (Map.Entry<String, Integer> map : set) {
            System.out.println("В нашей мапе: " + map.getKey() + " в кол-ве: " + map.getValue());
        }
        return null;
    }

    @Override
    public int getProductQuantity(String product) {
        Set<Map.Entry<String, Integer>> set = store.entrySet();
        for (Map.Entry<String, Integer> map : set) {
            if (product.equals(map.getKey()))
                System.out.println("В нашей мапе: " + map.getKey() + " в кол-ве: " + map.getValue());
        }
        return 0;
    }
}