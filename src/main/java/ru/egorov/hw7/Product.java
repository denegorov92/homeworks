package ru.egorov.hw7;

public class Product {
    private final Info drinkType;

    public Product(Info drinkType) {
        this.drinkType = drinkType;
    }

    public Info take() {
        return drinkType;
    }

    public String getName() {
        return drinkType.getName();
    }

    public int getPrice() {
        return drinkType.getPrice();
    }
}

