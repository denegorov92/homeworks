package ru.egorov.hw7;

import java.util.Scanner;

public class App {
    private static VM vm = new VM();

    public static void main(String[] args) {

        System.out.println("Меню: ");
        for (String line : vm.getDrinkTypes()) {
            System.out.println(line);
        }

        Scanner scan = new Scanner(System.in);
        printHelp();
        while (scan.hasNext()) {
            String command = scan.next();
            switch (command) {
                case "add": {
                    int money = scan.nextInt();
                    processAddMoney(money);
                    break;
                }
                case "get": {
                    int key = scan.nextInt();
                    processGetDrink(key);
                    break;
                }
                case "end": {
                    processEnd();
                    return;
                }
                default:
                    System.out.println("Неверный ввод");
            }
            scan.nextLine();
        }
    }

    private static void processAddMoney(int money) {
        System.out.println("Внесено: " + vm.addMoney(money));
    }

    private static void processGetDrink(int key) {
        Info drinkType = vm.giveMeADrink(key);
        if (drinkType != null) {
            System.out.println("Ваш выбор: " + drinkType.getName());
        } else {
            System.out.println("Произошла ошибка...");
        }
    }

    private static void processEnd() {
        System.out.println("Ваша сдача: " + vm.getChange());
    }

    private static void printHelp() {
        System.out.println("Введите 'add <количество>' для добавления купюр");
        System.out.println("Введите 'get <код напитка>' для получения напитка");
        System.out.println("Введите 'end' для получения сдачи");
    }
}

