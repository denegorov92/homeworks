package ru.egorov.hw9.animals;

public class App {
    public static void main(String[] args) {

        Lion lion = new Lion("King");
        lion.getName();
        Duck duck = new Duck("Donald");
        duck.getName();
        Dog dog = new Dog("Spike");
        dog.getName();
    }

}

abstract class Animal {

    private String name;

    public String Name() {
        return name;
    }

    public Animal(String name) {

        this.name = name;
    }

    public abstract void getName();
}