package ru.egorov.hw9.animals;

class Dog extends Animal implements Running, Swimming {

    public Dog(String name) {

        super(name);

    }

    public void getName() {

        System.out.println("Dog Name: " + Name());
    }

    @Override
    public void run() {

    }

    @Override
    public void swim() {

    }
}
