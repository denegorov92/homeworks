package ru.egorov.hw9.animals;

class Duck extends Animal implements Flying, Running, Swimming {

    public Duck(String name) {

        super(name);

    }

    public void getName() {

        System.out.println("Duck Name: " + Name());
    }

    @Override
    public void fly() {

    }

    @Override
    public void run() {

    }

    @Override
    public void swim() {

    }
}
