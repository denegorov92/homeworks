package ru.egorov.hw9.animals;

class Lion extends Animal implements Running, Swimming {


    public Lion(String name) {

        super(name);

    }

    public void getName() {

        System.out.println("Lion Name: " + Name());
    }

    @Override
    public void run() {

    }

    @Override
    public void swim() {

    }
}