package ru.egorov.hw9.human;

public interface HumanRunning {

    void longDistance();

    void shortDistance();
}

