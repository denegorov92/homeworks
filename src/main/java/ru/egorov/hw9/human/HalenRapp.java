package ru.egorov.hw9.human;

public class HalenRapp extends Human {
    int result = 0;

    @Override
    public void longDistance() {
        result = 120;
        System.out.println("Результат на длинной дистанции: " + result);
    }

    @Override
    public void shortDistance() {
        result = 15;
        System.out.println("Результат на короткой дистанции: " + result);
    }

    @Override
    public void brass() {
        result = 28;
        System.out.println("Результат брасом: " + result);
    }

    @Override
    public void freestyle() {
        result = 21;
        System.out.println("Результат вольным стилем: " + result);
    }
}