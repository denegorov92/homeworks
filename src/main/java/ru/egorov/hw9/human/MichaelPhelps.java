package ru.egorov.hw9.human;

public class MichaelPhelps extends Human {
    int result = 0;

    @Override
    public void longDistance() {
        result = 150;
        System.out.println("Результат на длинной дистанции: " + result);
    }

    @Override
    public void shortDistance() {
        result = 19;
        System.out.println("Результат на короткой дистанции: " + result);
    }

    @Override
    public void brass() {
        result = 24;
        System.out.println("Результат брасом: " + result);
    }

    @Override
    public void freestyle() {
        result = 19;
        System.out.println("Результат вольным стилем: " + result);
    }
}