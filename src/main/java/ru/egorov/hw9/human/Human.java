package ru.egorov.hw9.human;


abstract class Human implements HumanRunning, HumanSwimming {

    public static void main(String[] args) {

        MichaelPhelps michaelPhelps = new MichaelPhelps();
        System.out.println("Michael Phelps");
        michaelPhelps.longDistance();
        michaelPhelps.shortDistance();
        michaelPhelps.brass();
        michaelPhelps.freestyle();

        HalenRapp halenRapp = new HalenRapp();
        System.out.println("Halen Rapp");
        halenRapp.longDistance();
        halenRapp.shortDistance();
        halenRapp.brass();
        halenRapp.freestyle();

    }

    }


