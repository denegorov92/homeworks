package ru.egorov.hw22;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class App {
    public static void main(String[] args) {

        List<Person> person = new ArrayList<>();
        person.add(new Person("Denis", 25));
        person.add(new Person("Boris", 27));
        person.add(new Person("Artem", 30));
        person.add(new Person("Denis", 30));

        System.out.println("Список до сортировки: ");
        iterate(person);
        System.out.println("Список после сортировки по именам: ");
        Collections.sort(person, new PersonSuperComparator().nameComparator);
        iterate(person);
        System.out.println("Список после сортировки по именам и возрасту: ");
        Collections.sort(person, new PersonSuperComparator().ageComparator);
        iterate(person);
    }

    private static void iterate(List<Person> person) {
        Iterator i = person.iterator();
        while (i.hasNext()) {
            System.out.print(i.next());
        }
        System.out.println();
    }
}



