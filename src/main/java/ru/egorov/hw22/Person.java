package ru.egorov.hw22;

public class Person {
    String name;
    int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Имя персоны: " + name +
                " Возраст персоны: " + age + "\n";
    }
}
