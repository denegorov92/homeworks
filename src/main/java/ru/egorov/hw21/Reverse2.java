package ru.egorov.hw21;

import java.util.Arrays;

public class Reverse2 {
    static void returnReversArray(int[] arr) {
        int temp;
        for (int i = arr.length - 1, j = 0; i >= arr.length / 2; i--, j++) {
            temp = arr[j];
            arr[j] = arr[i];
            arr[i] = temp;
        }
    }

    public static void main(String[] args) {


        int[] arr = new int[10];
        arr[0] = 11;
        arr[1] = 22;
        arr[2] = 33;
        arr[3] = 44;
        arr[4] = 55;
        arr[5] = 66;
        arr[6] = 77;
        arr[7] = 88;
        arr[8] = 99;
        arr[9] = 100;

        System.out.println("Первоначальные элементы массива: ");
        for (int key : arr) {
            System.out.print(key + " ");
        }


        System.out.println("\nПереработанные элементы массива: ");
        returnReversArray(arr);
        for (int key : arr) {
            System.out.print(key + " ");
        }

        System.out.println("\n" + Arrays.toString(arr));
    }
}