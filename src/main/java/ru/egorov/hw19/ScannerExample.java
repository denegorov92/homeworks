package ru.egorov.hw19;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class ScannerExample {
    public static void main(String[] args) throws IOException {
        try (Scanner scanner = new Scanner(new File("products.txt"))) {
            System.out.printf("%-20s %-10s %-7s %s %n", "Наименование", "Кол-во", "Цена", "Стоимость");
            System.out.println("=================================================");
            Double Sum = 0.0;
            while (scanner.hasNext()) {
                String s = scanner.next();
                Float f1 = scanner.nextFloat();
                Float f2 = scanner.nextFloat();
                Float S = f1 * f2;
                System.out.printf("%-20s %.3f\t x  %.2f\t %8.2f\n", s, f1, f2, S);
                Sum = Sum + S;
            }
            System.out.println("=================================================");
            System.out.printf("%-40s %0,5.2f", "Итого:", Sum);
        }
    }
}